#!/bin/sh

sysrc rc_startmsgs="NO"
sysrc -f /boot/loader.conf boot_mute="YES"
sysrc -f /boot/loader.conf autoboot_delay=1
sysrc -f /boot/loader.conf verbose_loading="NO"
sysrc -f /boot/loader.conf loader_logo="beastiebw"

sed -i '' 's/run_rc_script ${_rc_elem} ${_boot}/run_rc_script ${_rc_elem} ${_boot} > \/dev\/null/g' /etc/rc

