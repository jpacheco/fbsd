#!/bin/sh

packs () {
    pkg install -y \
        doas \
        libXft \
        libXinerama \
        pkgconf \
        zsh \           # opcional
        xwallpaper \    # opcional
        pfetch \        # opcional
        xset \          # opcional
        setxkbmap \     # opcional
        xcape           # opcional
}

doasconf () {
    echo "permit nopass root" > /usr/local/etc/doas.conf
    echo "permit javier as root" >> /usr/local/etc/doas.conf
    echo "permit nopass javier" >> /usr/local/etc/doas.conf
}

userconf () {
    chsh -s zsh javier
}

echo "Installing some necesary packages to compile suckless tools..."
packs && echo "packages installed correctly..." || echo "Run this command as root, check your internet conection."

echo "make the doas configuration file..."
doasconf && echo "doas config file generated..." || echo "something was wrong..."

echo "changing the shell..."
userconf && echo "user now have zsh shell..." || echo "Is zsh installled?"
